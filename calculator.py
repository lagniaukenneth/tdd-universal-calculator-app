def convert_kph_to_mph(speed):
    return round((speed * 0.6213711922), 1)


def convert_kph_to_mps(speed):
    return round((speed * 0.27777777777778), 1)


def convert_kg_to_lbs(mass):
    return round((mass * 2.20462262), 1)


def convert_lbs_to_kg(mass):
    return round((mass * 0.45359237), 1)


def convert_m_to_ft(length):
    return round((length * 3.280839895), 1)


def convert_inches_to_cm(length):
    return round((length * 2.54), 1)


def convert_km_to_trips_around_the_moon(length):
    return round(88400, 1)
