from calculator import *
from unittest import TestCase


class TestCalculator(TestCase):
    def test_convert_kph_to_mph(self):

        speed_in_kph = 10
        expected_result = 6.2

        actual_result = convert_kph_to_mph(speed_in_kph)

        self.assertEqual(expected_result, actual_result)

    def test_convert_kph_to_mps(self):

        speed_in_kph = 10
        expected_result = 2.8

        actual_result = convert_kph_to_mps(speed_in_kph)

        self.assertEqual(expected_result, actual_result)

    def test_convert_kg_to_lbs(self):
        mass_in_kg = 10
        expected_result = 22.0

        actual_result = convert_kg_to_lbs(mass_in_kg)

        self.assertEqual(expected_result, actual_result)

    def test_convert_lbs_to_kg(self):
        mass_in_lbs = 10
        expected_result = 4.5

        actual_result = convert_lbs_to_kg(mass_in_lbs)

        self.assertEqual(expected_result, actual_result)

    def test_convert_m_to_ft(self):
        length_in_m = 10
        expected_result = 32.8

        actual_result = convert_m_to_ft(length_in_m)

        self.assertEqual(expected_result, actual_result)

    def test_convert_inches_to_cm(self):
        length_in_inches = 10
        expected_result = 25.4

        actual_result = convert_inches_to_cm(length_in_inches)

        self.assertEqual(expected_result, actual_result)

    def test_km_to_trips_around_the_moon(self):
        length_in_km = 88400
        expected_result = 88400

        actual_result = convert_km_to_trips_around_the_moon(length_in_km)

        self.assertEqual(expected_result, actual_result)




